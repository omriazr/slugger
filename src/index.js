export function slugger(...args){
    return args.map((str) => str.trim().replace(" ", "-")).join("-").replaceAll(' ','');
}
