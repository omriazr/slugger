import {slugger} from './index.js';


describe('testing slugger basic functionality', () => {
    /**
     * @it - unit tests can use the 'it' syntax
     */
    it('slugger can slug string with spaces', () => {
        expect(slugger('first second','third')).toEqual('first-second-third');
    })
    /**
     * @test - unit test can use the 'test' syntax
     */
    test('slugger can slug any number of spacy strings', () => {
        expect(slugger('first second','third')).toEqual('first-second-third');
    })

    test('slugger can slug any number of spacy strings', () => {
        expect(slugger("   rena  arfi ", "rapyd")).toEqual("rena-arfi-rapyd");
    })
})